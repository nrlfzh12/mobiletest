part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class PostLoginEvent extends AuthEvent {
  final RequestLogin data;
  const PostLoginEvent(this.data);
}
