import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:mobiletest/model/request/request_login.dart';
import 'package:mobiletest/model/response/response_login.dart';
import 'package:mobiletest/service/api_service_auth.dart';
import 'package:mobiletest/util/preference_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final ApiServiceAuth _apiServiceAuth = ApiServiceAuth();
  PreferencesHelper preps =
      PreferencesHelper(sharedPreferences: SharedPreferences.getInstance());
  AuthBloc() : super(AuthInitial()) {
    on<PostLoginEvent>((event, emit) async {
      emit(PostLoginLoading());
      try {
        // log('event.data.email');
        log("${event.data.email} email");
        log("${event.data.password} emailpassword");
        final response = await _apiServiceAuth.postLogin(
            email: event.data.email, password: event.data.password);

        emit(PostLoginSuccess(ResponseLogin.fromJson(response.data)));
        preps.setStringSharedPref('token', response.data['token']);
        log(await preps.getToken);
      } on DioError catch (e) {
        emit(PostLoginError(e.response?.data['error']));
      } catch (e) {
        emit(PostLoginError(e.toString()));
      }
      // TODO: implement event handler
    });
  }
}
