part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}

class PostLoginLoading extends AuthState {}

class PostLoginSuccess extends AuthState {
  final ResponseLogin data;
  const PostLoginSuccess(this.data);
}

class PostLoginError extends AuthState {
  final String error;
  const PostLoginError(this.error);
}
