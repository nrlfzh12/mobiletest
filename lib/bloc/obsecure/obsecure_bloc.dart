import 'dart:developer';

import 'package:flutter_bloc/flutter_bloc.dart';

class ObsecureBloc extends Cubit {
  ObsecureBloc() : super(null);

  bool isObscure = true;

  void changeObsecure() {
    emit(isObscure = !isObscure);
    log(isObscure.toString());
  }
}
