import 'package:dio/dio.dart';
import 'package:mobiletest/helper/constants.dart';
import 'package:mobiletest/util/network_helper.dart';

class ApiServiceAuth {
  NetworkHelper dio = NetworkHelper();

  Future<Response> postLogin(
      {required String email, required String password}) async {
    final response = await dio.post('${baseApi}api/login', data: {
      "email": email,
      "password": password,
    });
    return response;
  }
}
