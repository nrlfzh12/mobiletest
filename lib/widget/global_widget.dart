import 'package:flutter/material.dart';

class GlobalWidget {
  static showAlertDialog(BuildContext context, String message) {
    // set up the buttons
    // Widget cancelButton = MainBottom(
    //   function: () {
    //     Navigator.pop(context);
    //   },
    //   title: 'Cancel',
    // );
    // Widget continueButton = MainBottom(
    //   function: () {
    //     Navigation.back();
    //   },
    //   title: 'Ok',
    // );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      title: Column(
        // ignore: prefer_const_literals_to_create_immutables
        children: [
          // Image.asset('assets/images/ic_verified.png'),
          const SizedBox(
            height: 15,
          ),
          // const Text(
          //   'Error',
          //   textAlign: TextAlign.center,
          //   // style: TextHelper.title18,
          // )
        ],
      ),
      content: Text(
        message,
        textAlign: TextAlign.center,
      ),
      actions: [
        Container(
            margin: const EdgeInsets.only(left: 16, right: 16, bottom: 10),
            width: double.infinity,
            child: TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text('Ok'))),
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  // static showAlertDialogTwoButton(
  //     {BuildContext? context, String? message, Function()? onTap}) {
  //   // set up the buttons
  //   Widget cancelButton = MainButtonOutlined(
  //     function: () {
  //       Navigator.pop(context!);
  //     },
  //     title: 'Tidak',
  //   );
  //   Widget continueButton = MainBottom(
  //     function: onTap,
  //     title: 'Ok',
  //   );

  //   // set up the AlertDialog
  //   AlertDialog alert = AlertDialog(
  //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
  //     title: Column(
  //       // ignore: prefer_const_literals_to_create_immutables
  //       children: [
  //         // Image.asset('assets/images/ic_verified.png'),
  //         const SizedBox(
  //           height: 15,
  //         ),
  //         const Text(
  //           'Hapus Postingan',
  //           textAlign: TextAlign.center,
  //           style: TextStyle(fontWeight: FontWeight.bold),
  //         )
  //       ],
  //     ),
  //     content: Text(
  //       'Apakah anda yakin akan menghapus $message?',
  //       textAlign: TextAlign.center,
  //     ),
  //     actions: [
  //       SizedBox(
  //         height: 50,
  //         child: Row(
  //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //           children: [
  //             Container(
  //                 margin:
  //                     const EdgeInsets.only(left: 16, right: 16, bottom: 10),
  //                 width: 120,
  //                 child: cancelButton),
  //             Container(
  //                 margin: const EdgeInsets.only(right: 16, bottom: 10),
  //                 width: 120,
  //                 child: continueButton),
  //           ],
  //         ),
  //       ),
  //     ],
  //   );

  //   // show the dialog
  //   showDialog(
  //     context: context!,
  //     builder: (BuildContext context) {
  //       return alert;
  //     },
  //   );
  // }

  // skeletonListHorizontal() {
  //   return SizedBox(
  //     height: 280,
  //     child: ListView.builder(
  //         shrinkWrap: true,
  //         scrollDirection: Axis.horizontal,
  //         padding: EdgeInsets.zero,
  //         itemCount: 4,
  //         itemBuilder: (context, index) {
  //           return Padding(
  //             padding: const EdgeInsets.all(16.0),
  //             child: Card(
  //               shape: RoundedRectangleBorder(
  //                   borderRadius: BorderRadius.circular(20)),
  //               child: Shimmer.fromColors(
  //                 baseColor: Colors.grey.withOpacity(0.2),
  //                 highlightColor: Colors.white,
  //                 period: const Duration(milliseconds: 800),
  //                 child: Column(
  //                   crossAxisAlignment: CrossAxisAlignment.start,
  //                   children: [
  //                     Container(
  //                       width: 300,
  //                       height: 150,
  //                       decoration: const BoxDecoration(
  //                           color: Colors.grey,
  //                           borderRadius: BorderRadius.only(
  //                               topLeft: Radius.circular(20),
  //                               topRight: Radius.circular(20))),
  //                     ),
  //                     const SizedBox(
  //                       height: 16.0,
  //                     ),
  //                     Padding(
  //                       padding: const EdgeInsets.all(8.0),
  //                       child: Row(
  //                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                         crossAxisAlignment: CrossAxisAlignment.start,
  //                         children: [
  //                           Column(
  //                             crossAxisAlignment: CrossAxisAlignment.start,
  //                             children: [
  //                               Container(
  //                                 width: 100,
  //                                 height: 10,
  //                                 color: Colors.grey,
  //                               ),
  //                               const SizedBox(
  //                                 height: 10,
  //                               ),
  //                               Container(
  //                                 width: 100,
  //                                 height: 10,
  //                                 color: Colors.grey,
  //                               ),
  //                             ],
  //                           ),
  //                           Container(
  //                             width: 100,
  //                             height: 10,
  //                             color: Colors.grey,
  //                           ),
  //                         ],
  //                       ),
  //                     ),
  //                     const SizedBox(
  //                       height: 16,
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //             ),
  //           );
  //         }),
  //   );
  // }

  // skeletonList() {
  //   return ListView.builder(
  //       shrinkWrap: true,
  //       itemCount: 5,
  //       itemBuilder: (context, index) {
  //         return Padding(
  //           padding: const EdgeInsets.all(16.0),
  //           child: Shimmer.fromColors(
  //             baseColor: Colors.grey.withOpacity(0.2),
  //             highlightColor: Colors.white,
  //             period: const Duration(milliseconds: 800),
  //             child: Column(
  //               children: [
  //                 Row(
  //                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                   children: [
  //                     Column(
  //                       children: [
  //                         Container(
  //                           width: 100,
  //                           height: 10,
  //                           color: Colors.grey,
  //                         ),
  //                         const SizedBox(
  //                           height: 10,
  //                         ),
  //                         Container(
  //                           width: 100,
  //                           height: 10,
  //                           color: Colors.grey,
  //                         ),
  //                       ],
  //                     ),
  //                     Container(
  //                       width: 100,
  //                       height: 10,
  //                       color: Colors.grey,
  //                     ),
  //                   ],
  //                 ),
  //                 const SizedBox(
  //                   height: 16,
  //                 ),
  //                 Container(
  //                   width: double.infinity,
  //                   height: 66,
  //                   color: Colors.grey,
  //                 ),
  //               ],
  //             ),
  //           ),
  //         );
  //       });
  // }
}
