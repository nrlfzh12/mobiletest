import 'package:flutter/material.dart';
import 'package:mobiletest/helper/constants.dart';
import 'package:mobiletest/helper/navigation.dart';
import 'package:mobiletest/helper/router.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileDrawerWidget extends StatelessWidget {
  const ProfileDrawerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 20.0,
            ),
            Container(
              margin: const EdgeInsets.all(16),
              width: 230,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ListTile(
                    leading: const CircleAvatar(
                      backgroundImage: NetworkImage(
                        "https://i.ibb.co/PGv8ZzG/me.jpg",
                      ),
                    ),
                    title: RichText(
                        text: const TextSpan(children: [
                      TextSpan(
                          text: 'Angga ',
                          style: TextStyle(
                              color: CustomColors.BASE_COLOR,
                              fontSize: 14,
                              fontWeight: FontWeight.w600)),
                      TextSpan(
                        text: 'Praja',
                        style: TextStyle(
                            color: CustomColors.BASE_COLOR,
                            fontSize: 14,
                            fontWeight: FontWeight.w400),
                      ),
                    ])),
                    subtitle: const Text("Membership BBLK"),
                  ),
                  ListTile(
                    title: const Text('Profile Saya'),
                    trailing: const Icon(
                      Icons.arrow_forward_ios,
                      color: CustomColors.BASE_COLOR,
                      size: 18,
                    ),
                    onTap: () {
                      Navigation.pushNoData(profileRoute);
                    },
                  ),
                  // const Divider(height: 1),
                  ListTile(
                    title: const Text('Pengaturan'),
                    trailing: const Icon(Icons.arrow_forward_ios,
                        color: CustomColors.BASE_COLOR, size: 18),
                    onTap: () {
                      Navigation.pushNoData(profileRoute);
                    },
                  ),

                  Container(
                    margin: const EdgeInsets.all(16),
                    width: double.infinity,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: CustomColors.RED,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20))),
                        onPressed: () async {
                          SharedPreferences pref =
                              await SharedPreferences.getInstance();
                          pref.clear();

                          Navigation.pushNoData(loginRoute);
                        },
                        child: const Text(
                          'Logout',
                          style: TextStyle(color: CustomColors.WHITE_SMOKE),
                        )),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 40.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'Ikuti kami di',
                  style:
                      TextStyle(color: CustomColors.BASE_COLOR, fontSize: 16),
                ),
                const SizedBox(
                  width: 10.0,
                ),
                const Icon(
                  Icons.facebook,
                  size: 18,
                  color: CustomColors.BASE_COLOR2,
                ),
                const SizedBox(
                  width: 5.0,
                ),
                Image.asset(
                  'assets/images/instagram.png',
                  width: 15,
                  scale: 3,
                ),
                const SizedBox(
                  width: 5.0,
                ),
                Image.asset(
                  'assets/images/twitter.png',
                  width: 15,
                  scale: 1,
                ),
              ],
            ),
            Align(
              heightFactor: 20,
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Text(
                    'FAQ',
                    style: TextStyle(
                        color: CustomColors.GREY,
                        fontSize: 14,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Text(
                    'Term and Conditions',
                    style: TextStyle(
                        color: CustomColors.GREY,
                        fontSize: 14,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
