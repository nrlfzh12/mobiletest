import 'package:flutter/material.dart';
import 'package:mobiletest/screen/home/home_screen.dart';
import 'package:mobiletest/screen/login/login_screen.dart';
import 'package:mobiletest/screen/profile/profile_screen.dart';
import 'package:mobiletest/screen/splash/splash_screen.dart';

const String splashRoute = '/splash';
const String loginRoute = '/login';
const String homeRoute = '/home';
const String registerRoute = '/register';
const String profileRoute = '/profile';

Map<String, Widget Function(BuildContext)> myRoutes = <String, WidgetBuilder>{
  splashRoute: (context) => const SplashScreen(),
  loginRoute: (context) => const LoginScreen(),
  homeRoute: (context) => const HomeScreen(),
  profileRoute: (context) => const ProfileScreen(),
};
