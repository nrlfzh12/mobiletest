import 'dart:ui';

class CustomColors {
  static const Color BASE_COLOR = Color(0xff1D334F);
  static const Color BASE_COLOR2 = Color(0xff002060);
  static const Color WHITE = Color(0xffFFFFFF);
  static const Color GREY = Color(0xffBEBEBE);
  static const Color ORANGE = Color(0xffFF7200);
  static const Color TOGGLE = Color(0xff00D9D5);
  static const Color RED = Color(0xffEB0004);
  static const Color WHITE_SMOKE = Color(0xffFFFFFF);
}

String baseApi = 'https://reqres.in/';
