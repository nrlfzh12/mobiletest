import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:mobiletest/helper/navigation.dart';
import 'package:mobiletest/helper/router.dart';
import 'package:mobiletest/util/preference_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

String finalToken = '';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  String tokenFirebase = '';
  SharedPreferences? pref;
  PreferencesHelper preferencesHelper =
      PreferencesHelper(sharedPreferences: SharedPreferences.getInstance());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        // backgroundColor: ColorHelper.white,
        body: Stack(
          children: <Widget>[
            /// Background
            Container(
              decoration: const BoxDecoration(
                  // color: ColorHelper.baseColor,
                  ),
            ),
            const Padding(
              padding: EdgeInsets.all(36.0),
              child: Center(
                child: SizedBox(
                  child: FlutterLogo(
                    size: 50,
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  @override
  void initState() {
    getValidationData(); // TODO: implement initState

    _navigateToAfterSplash();

    super.initState();
  }

  Future<String> getValidationData() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var obtainedToken = sharedPreferences.getString("token");
    var role = sharedPreferences.getString("role");
    setState(() {
      finalToken = obtainedToken ?? '';
    });

    return obtainedToken ?? '';
  }

  _navigateToAfterSplash() async {
    log(finalToken.toString());
    Timer(const Duration(seconds: 1), () {
      if (finalToken == '') {
        Navigation.pushNoData(loginRoute);
      } else {
        Navigation.pushNoData(homeRoute);
      }
    });
  }
}
