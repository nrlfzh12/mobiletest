// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:mobiletest/helper/constants.dart';
import 'package:mobiletest/widget/animated_toggle.dart';
import 'package:mobiletest/widget/custom_text_field_form.dart';
import 'package:mobiletest/widget/profile_widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _endDrawerKey = GlobalKey();
  int _toggleValue = 0;
  int indexFilter = 0;

  List<String> list = [
    'All Product',
    'Layanan Kesehatan',
    'Alat Kesehatan',
    'Obat - obatan'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _endDrawerKey,
      endDrawerEnableOpenDragGesture: true,
      endDrawer: const Drawer(
        child: ProfileDrawerWidget(),
      ),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.menu,
                color: CustomColors.BASE_COLOR,
              ),
              onPressed: () {
                _endDrawerKey.currentState!.openEndDrawer();
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
        actions: [
          const Icon(
            Icons.shopping_cart,
            color: CustomColors.BASE_COLOR,
          ),
          const SizedBox(
            width: 20.0,
          ),
          Container(
            margin: const EdgeInsets.only(right: 16, top: 16),
            child: Stack(children: const [
              Icon(
                Icons.notifications,
                color: CustomColors.BASE_COLOR,
              ),
              Positioned(
                // draw a red marble
                top: 0.0,
                right: 3.0,
                child: Icon(Icons.brightness_1,
                    size: 8.0, color: Colors.redAccent),
              )
            ]),
          )

          // Icon(Icons.abc),
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      color: Colors.transparent,
                      height: 180.0,
                      width: double.infinity,
                      child: Stack(children: <Widget>[
                        Positioned(
                          // bottom: 20,
                          right: 0,
                          left: 0,
                          top: 30,
                          child: Material(
                            elevation: 8,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(16)),
                            child: Container(
                              // margin: const EdgeInsets.only(left: 16, right: 16),
                              height: 140,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                image: const DecorationImage(
                                    image: AssetImage(
                                      'assets/images/bg_card.png',
                                    ),
                                    fit: BoxFit.cover),
                                borderRadius: BorderRadius.circular(16),
                              ),
                              child: ListTile(
                                title: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    const SizedBox(
                                      height: 10.0,
                                    ),
                                    RichText(
                                        text: const TextSpan(children: [
                                      TextSpan(
                                          text: 'Solusi, ',
                                          style: TextStyle(
                                              color: CustomColors.BASE_COLOR,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w500)),
                                      TextSpan(
                                          text: 'Kesehatan Anda',
                                          style: TextStyle(
                                              color: CustomColors.BASE_COLOR,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold)),
                                    ])),
                                    const SizedBox(
                                      height: 10.0,
                                    ),
                                    const SizedBox(
                                      width: 200,
                                      child: Text(
                                          'Update informasi seputar kesehatan semua bisa disini !',
                                          style: TextStyle(
                                              color: Color(0xff597393),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400)),
                                    ),
                                    const SizedBox(
                                      height: 10.0,
                                    ),
                                    ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            backgroundColor:
                                                CustomColors.BASE_COLOR,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        onPressed: () {
                                          // log('ttt');
                                        },
                                        child: const Text('Selengkapnya')),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                            // bottom: 100,
                            right: -6,
                            top: -30,
                            child: SizedBox(
                              child: Image.asset(
                                'assets/images/schedule.png',
                                width: 160,
                                height: 210,
                              ),
                            )),
                        Positioned(
                            bottom: 20,
                            right: 20,
                            child: SizedBox(
                              child: Image.asset(
                                'assets/images/line.png',
                                width: 100,
                                height: 20,
                              ),
                            )),
                      ])),
                  const SizedBox(
                    height: 10.0,
                  ),
                  Container(
                      color: Colors.transparent,
                      height: 200.0,
                      width: double.infinity,
                      child: Stack(children: <Widget>[
                        Positioned(
                          // bottom: 20,
                          right: 0,
                          left: 0,
                          top: 30,
                          child: Material(
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(16)),
                            child: Container(
                              // margin: const EdgeInsets.only(left: 16, right: 16),
                              height: 140,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                color: const Color(0xffFFFFFF),
                                borderRadius: BorderRadius.circular(16),
                              ),
                              child: ListTile(
                                title: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    const SizedBox(
                                      height: 10.0,
                                    ),
                                    RichText(
                                        text: const TextSpan(children: [
                                      TextSpan(
                                          text: 'Layanan Khusus ',
                                          style: TextStyle(
                                              color: CustomColors.BASE_COLOR,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w500)),
                                      //         fontWeight: FontWeight.bold)),
                                    ])),
                                    const SizedBox(
                                      height: 10.0,
                                    ),
                                    const SizedBox(
                                      width: 200,
                                      child: Text(
                                          'Tes Covid 19, Cegah Corona Sedini Mungkin',
                                          style: TextStyle(
                                              color: Color(0xff597393),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400)),
                                    ),
                                    const SizedBox(
                                      height: 10.0,
                                    ),
                                    TextButton.icon(
                                      onPressed: () {},
                                      icon: const Text('Daftar Tes',
                                          style: TextStyle(
                                              color: CustomColors.BASE_COLOR,
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold)),
                                      label: const Icon(
                                        Icons.arrow_forward,
                                        size: 20,
                                        color: CustomColors.BASE_COLOR,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                            // bottom: 100,
                            right: 10,
                            top: 0,
                            child: SizedBox(
                              child: Image.asset(
                                'assets/images/medicine.png',
                                width: 119,
                                height: 120,
                              ),
                            )),
                      ])),
                  Container(
                      color: Colors.transparent,
                      height: 180.0,
                      width: double.infinity,
                      child: Stack(children: <Widget>[
                        Positioned(
                          // bottom: 20,
                          right: 0,
                          left: 0,
                          top: 30,
                          child: Material(
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(16)),
                            child: Container(
                              height: 120,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                color: const Color(0xffFFFFFF),
                                borderRadius: BorderRadius.circular(16),
                              ),
                              child: ListTile(
                                title: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    SizedBox(
                                      width: 200,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          const SizedBox(
                                            height: 10.0,
                                          ),
                                          RichText(
                                              text: const TextSpan(children: [
                                            TextSpan(
                                                text: 'Track Pemeriksaan ',
                                                style: TextStyle(
                                                    color:
                                                        CustomColors.BASE_COLOR,
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.w500)),
                                          ])),
                                          const SizedBox(
                                            height: 10.0,
                                          ),
                                          const SizedBox(
                                            width: 200,
                                            child: Text(
                                                'Kamu dapat mengecek progress pemeriksaanmu disini',
                                                style: TextStyle(
                                                    color: Color(0xff597393),
                                                    fontSize: 12,
                                                    fontWeight:
                                                        FontWeight.w400)),
                                          ),
                                          const SizedBox(
                                            height: 10.0,
                                          ),
                                          Container(
                                            child: Row(
                                              children: const [
                                                Text('Track',
                                                    style: TextStyle(
                                                        color: CustomColors
                                                            .BASE_COLOR,
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.bold)),
                                                SizedBox(
                                                  width: 5.0,
                                                ),
                                                Icon(
                                                  Icons.arrow_forward,
                                                  size: 20,
                                                  color:
                                                      CustomColors.BASE_COLOR,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                            // bottom: 100,
                            left: 10,
                            top: 0,
                            child: SizedBox(
                              child: Image.asset(
                                'assets/images/glass.png',
                                width: 100,
                                height: 100,
                              ),
                            )),
                      ])),
                  Row(
                    children: [
                      Material(
                        elevation: 2,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(60)),
                        child: Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(80),
                              color: const Color(0xffFFFFFF)),
                          child: const Icon(
                            Icons.tune,
                            color: CustomColors.BASE_COLOR,
                            size: 20,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 20.0,
                      ),
                      Expanded(
                          child: CustomTextFieldForm(
                        hintText: 'Search',
                        suffixIcon: const Icon(Icons.search),
                      )),
                    ],
                  ),
                  const SizedBox(
                    height: 30.0,
                  ),
                  SizedBox(
                    height: 35.0,
                    child: ListView.builder(
                      itemCount: list.length,
                      scrollDirection: Axis.horizontal,
                      padding: EdgeInsets.zero,
                      clipBehavior: Clip.none,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            setState(() {
                              indexFilter = index;
                            });
                          },
                          child: Container(
                              margin:
                                  const EdgeInsets.only(right: 10, left: 10),
                              padding: const EdgeInsets.only(
                                  left: 25, right: 25, top: 10, bottom: 10),
                              // height: 20,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.1),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: const Offset(
                                          0, 3), // changes position of shadow
                                    ),
                                  ],
                                  color: indexFilter == index
                                      ? CustomColors.BASE_COLOR
                                      : CustomColors.WHITE),
                              child: Text(list[index],
                                  style: TextStyle(
                                      color: indexFilter == index
                                          ? Colors.white
                                          : CustomColors.BASE_COLOR,
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold))),
                        );
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  SizedBox(
                    height: 190.0,
                    child: ListView.builder(
                      itemCount: 10,
                      scrollDirection: Axis.horizontal,
                      padding: EdgeInsets.zero,
                      clipBehavior: Clip.none,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Material(
                            elevation: 1,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12)),
                            child: Container(
                              width: 160,
                              height: 180,
                              decoration: BoxDecoration(
                                  color: CustomColors.WHITE,
                                  borderRadius: BorderRadius.circular(12)),
                              child: Column(
                                children: [
                                  Container(
                                    margin: const EdgeInsets.only(
                                      left: 10,
                                      right: 10,
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Icon(
                                          Icons.star,
                                          color: Colors.yellow[400],
                                        ),
                                        const Text(
                                          '5',
                                          style: TextStyle(
                                              color: CustomColors.GREY),
                                        )
                                      ],
                                    ),
                                  ),
                                  Image.asset(
                                    'assets/images/mikroskop.png',
                                    height: 80,
                                  ),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Row(
                                      children: const [
                                        Text(
                                          'Suntik Steril',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 5.0,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 8),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        const Text(
                                          'Rp. 10.000',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12,
                                              color: Color(0xffFF7200)),
                                        ),
                                        Container(
                                          padding: const EdgeInsets.all(5),
                                          decoration: BoxDecoration(
                                              color: const Color(0xffB3FFCB),
                                              borderRadius:
                                                  BorderRadius.circular(8)),
                                          child: const Text(
                                            'Ready Stock',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontSize: 10,
                                                color: Color(0xff007025)),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  const Text('Pilih Tipe Layanan Kesehatan Anda',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                          color: CustomColors.BASE_COLOR)),
                  const SizedBox(
                    height: 10.0,
                  ),
                  AnimatedToggle(
                    values: const ['Satuan', 'Pemeriksaan'],
                    onToggleCallback: (value) {
                      setState(() {
                        _toggleValue = value;
                      });
                    },
                    buttonColor: CustomColors.TOGGLE,
                    backgroundColor: CustomColors.WHITE,
                    textColor: CustomColors.BASE_COLOR,
                  ),
                  Material(
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12)),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 160,
                        decoration: BoxDecoration(
                            color: CustomColors.WHITE,
                            borderRadius: BorderRadius.circular(12)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 16),
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width / 2,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  // mainAxisSize: MainAxisSize.min,
                                  children: [
                                    const SizedBox(
                                      height: 15.0,
                                    ),
                                    const Text(
                                        'PCR Swab Test (Drive Thru) Hasil 1 Hari Kerja',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 14,
                                            color: CustomColors.BASE_COLOR)),
                                    const SizedBox(
                                      height: 10.0,
                                    ),
                                    const Text('Rp. 1.400.000',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 14,
                                            color: CustomColors.ORANGE)),
                                    const SizedBox(
                                      height: 20.0,
                                    ),
                                    Row(
                                      children: [
                                        const SizedBox(
                                          width: 4.0,
                                        ),
                                        Image.asset(
                                            'assets/images/hospital.png'),
                                        const SizedBox(
                                          width: 10.0,
                                        ),
                                        const Text('Lenmarc Surabaya',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontSize: 14,
                                                color:
                                                    CustomColors.BASE_COLOR)),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 5.0,
                                    ),
                                    Row(
                                      children: const [
                                        Icon(
                                          Icons.location_on,
                                          size: 20,
                                        ),
                                        SizedBox(
                                          width: 9.0,
                                        ),
                                        Text(
                                          'Dukuh Pakis, Surabaya',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 12,
                                              color: CustomColors.GREY),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height: 160,
                              width: 120,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(18),
                                  color: Colors.amber,
                                  image: const DecorationImage(
                                      image: AssetImage(
                                          'assets/images/image1.png'),
                                      fit: BoxFit.fill)),
                              // child: Image.asset('assets/images/image1.png'),s
                            ),
                          ],
                        ),
                      )),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Material(
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12)),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 160,
                        decoration: BoxDecoration(
                            color: CustomColors.WHITE,
                            borderRadius: BorderRadius.circular(12)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 16),
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width / 2,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  // mainAxisSize: MainAxisSize.min,
                                  children: [
                                    const SizedBox(
                                      height: 15.0,
                                    ),
                                    const Text(
                                        'PCR Swab Test (Drive Thru) Hasil 1 Hari Kerja',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 14,
                                            color: CustomColors.BASE_COLOR)),
                                    const SizedBox(
                                      height: 10.0,
                                    ),
                                    const Text('Rp. 1.400.000',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 14,
                                            color: CustomColors.ORANGE)),
                                    const SizedBox(
                                      height: 20.0,
                                    ),
                                    Row(
                                      children: [
                                        const SizedBox(
                                          width: 4.0,
                                        ),
                                        Image.asset(
                                            'assets/images/hospital.png'),
                                        const SizedBox(
                                          width: 10.0,
                                        ),
                                        const Text('Lenmarc Surabaya',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontSize: 14,
                                                color:
                                                    CustomColors.BASE_COLOR)),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 5.0,
                                    ),
                                    Row(
                                      children: const [
                                        Icon(
                                          Icons.location_on,
                                          size: 20,
                                        ),
                                        SizedBox(
                                          width: 9.0,
                                        ),
                                        Text(
                                          'Dukuh Pakis, Surabaya',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 12,
                                              color: CustomColors.GREY),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height: 160,
                              width: 120,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(18),
                                  color: Colors.amber,
                                  image: const DecorationImage(
                                      image: AssetImage(
                                          'assets/images/image2.png'),
                                      fit: BoxFit.fill)),
                              // child: Image.asset('assets/images/image1.png'),s
                            ),
                          ],
                        ),
                      )),
                  const SizedBox(
                    height: 30.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/images/more.png',
                        width: 16,
                      ),
                      const SizedBox(
                        width: 10.0,
                      ),
                      const Text('Tampilkan Lebih Banyak',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 14,
                              color: CustomColors.GREY)),
                    ],
                  ),
                  const SizedBox(
                    height: 30.0,
                  ),
                ],
              ),
            ),
            Container(
              height: 107,
              width: MediaQuery.of(context).size.width,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/pola.png'),
                      fit: BoxFit.cover),
                  color: CustomColors.BASE_COLOR2),
              child: Stack(children: [
                Positioned(
                    top: 20,
                    right: 90,
                    child: Image.asset(
                      'assets/images/ornamen.png',
                      width: 40,
                      height: 42,
                    )),
                const Positioned(
                    top: 40,
                    left: 30,
                    child: SizedBox(
                      width: 172,
                      child: Text('Ingin mendapat update dari kami ?',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: CustomColors.WHITE)),
                    )),
                Positioned(
                    top: 40,
                    right: -20,
                    child: SizedBox(
                        width: 172,
                        child: TextButton.icon(
                          onPressed: () {},
                          icon: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: const [
                              Text('Dapatkan',
                                  style: TextStyle(
                                      color: CustomColors.WHITE,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400)),
                              Text(' Aplikasi',
                                  style: TextStyle(
                                      color: CustomColors.WHITE,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400)),
                            ],
                          ),
                          label: const Icon(
                            Icons.arrow_forward,
                            size: 20,
                            color: CustomColors.WHITE,
                          ),
                        ))),
              ]),
            )
          ],
        ),
      ),
    );
  }
}
