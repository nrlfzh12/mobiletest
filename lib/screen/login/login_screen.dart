// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'dart:developer';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobiletest/bloc/auth/auth_bloc.dart';
import 'package:mobiletest/bloc/obsecure/obsecure_bloc.dart';
import 'package:mobiletest/helper/constants.dart';
import 'package:mobiletest/helper/navigation.dart';
import 'package:mobiletest/helper/router.dart';
import 'package:mobiletest/model/request/request_login.dart';
import 'package:mobiletest/widget/custom_text.dart';
import 'package:mobiletest/widget/custom_text_field_form.dart';
import 'package:mobiletest/widget/global_widget.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final AuthBloc _authBloc = AuthBloc();
  final ObsecureBloc _obsecureBloc = ObsecureBloc();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Stack(
          children: [
            // Positioned(
            //     right: 0,
            //     child: Image.asset(
            //       Images.BACKGROUND,
            //       scale: 4.5,
            //     )),
            Padding(
              padding: const EdgeInsets.all(32.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 40.0,
                  ),
                  RichText(
                      text: const TextSpan(children: [
                    TextSpan(
                        text: 'Hai, ',
                        style: TextStyle(
                            color: CustomColors.BASE_COLOR,
                            fontSize: 28,
                            fontWeight: FontWeight.w500)),
                    TextSpan(
                        text: 'Selamat Datang',
                        style: TextStyle(
                            color: CustomColors.BASE_COLOR,
                            fontSize: 28,
                            fontWeight: FontWeight.bold)),
                  ])),
                  const Text('Silahkan login untuk melanjutkan',
                      style: TextStyle(
                          color: CustomColors.BASE_COLOR,
                          fontSize: 12,
                          fontWeight: FontWeight.w600)),
                  Container(
                    width: double.infinity,
                    alignment: Alignment.topLeft,
                    child: Image.asset(
                      'assets/images/login_logo.png',
                      scale: 3.5,
                    ),
                  ),
                  // const SizedBox(
                  //   height: 16.0,
                  // ),
                  // const Text(
                  //   'NahNu App',
                  //   style: TextStyle(fontSize: 34, fontWeight: FontWeight.bold),
                  // ),
                  // const SizedBox(
                  //   height: 10.0,
                  // ),
                  CustomText(
                    text: 'Email',
                    color: CustomColors.BASE_COLOR,
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                  const SizedBox(
                    height: 8.0,
                  ),
                  CustomTextFieldForm(
                    keyboardType: TextInputType.text,
                    // color: ,
                    isDense: false,
                    isAll: true,
                    hintText: 'Masukkan email anda',
                    controller: _username,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Email tidak boleh kosong';
                      }
                      return '';
                    },
                    onSaved: (value) {
                      // _payload.fullName = value;
                    },
                  ),
                  const SizedBox(
                    height: 40.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CustomText(
                        text: 'Password anda',
                        color: CustomColors.BASE_COLOR,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                      CustomText(
                        text: 'Lupa Password anda ?',
                        color: const Color(0xff597393),
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 8.0,
                  ),
                  BlocBuilder<ObsecureBloc, dynamic>(
                    bloc: _obsecureBloc,
                    builder: (context, state) {
                      return CustomTextFieldForm(
                        keyboardType: TextInputType.text,
                        // color: AppUtil().parseHexColor(CustomColors.MORTAR),
                        isDense: false,
                        isObscure: _obsecureBloc.isObscure,
                        // isAll: true,
                        suffixIcon: GestureDetector(
                          onTap: () {
                            log('kesini');
                            _obsecureBloc.changeObsecure();
                          },
                          child: Image.asset(
                            'assets/images/visibility.png',
                            width: 15,
                            height: 15,
                            scale: 3,
                          ),
                        ),
                        hintText: 'Masukkan password anda',

                        controller: _password,

                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Password harus di isi!';
                          }
                          return '';
                        },
                        onSaved: (value) {
                          // _payload.fullName = value;
                        },
                      );
                    },
                  ),

                  const SizedBox(
                    height: 40.0,
                  ),

                  BlocConsumer<AuthBloc, AuthState>(
                    bloc: _authBloc,
                    listener: (context, state) {
                      if (state is PostLoginSuccess) {
                        Navigation.pushNoData(homeRoute);
                      } else if (state is PostLoginError) {
                        GlobalWidget.showAlertDialog(context, state.error);
                        log(state.error.toString());
                      }
                      // TODO: implement listener
                    },
                    builder: (context, state) {
                      if (state is PostLoginLoading) {
                        return InkWell(
                          onTap: () {},
                          child: Container(
                            padding: const EdgeInsets.only(right: 12),
                            width: double.infinity,
                            height: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: const Color(0xff002060)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                const Text('Loading...',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600)),
                                SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width / 3.5,
                                ),
                                const Icon(Icons.arrow_forward,
                                    color: Colors.white)
                              ],
                            ),
                          ),
                        );
                      } else {
                        return InkWell(
                          onTap: () {
                            log("${_username.text} username");
                            log("${_password.text} username");
                            _authBloc.add(PostLoginEvent(RequestLogin(
                                email: _username.text,
                                password: _password.text)));
                          },
                          child: Container(
                            // margin: const EdgeInsets.all(10),
                            padding: const EdgeInsets.only(right: 12),
                            width: double.infinity,
                            height: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: const Color(0xff002060)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                const Text('Login',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600)),
                                SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width / 3.5,
                                ),
                                const Icon(Icons.arrow_forward,
                                    color: Colors.white)
                              ],
                            ),
                          ),
                        );
                      }
                    },
                  ),
                  const SizedBox(
                    height: 30.0,
                  ),
                  Center(
                    child: RichText(
                        text: TextSpan(children: [
                      TextSpan(
                          text: 'Belum punya akun ? ',
                          style: TextStyle(
                              color: Colors.grey.withOpacity(0.6),
                              fontSize: 14,
                              fontWeight: FontWeight.w400)),
                      TextSpan(
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            Navigation.pushNoData(registerRoute);
                          },
                        text: 'Daftar Sekarang',
                        style: const TextStyle(
                            color: CustomColors.BASE_COLOR,
                            fontSize: 14,
                            fontWeight: FontWeight.w600),
                      ),
                    ])),
                  ),
                  const SizedBox(
                    height: 30.0,
                  ),
                  Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Icon(
                        Icons.copyright,
                        size: 15,
                        color: Color(0xffBEBEBE),
                      ),
                      Text(
                        '  SILK. all right reserved.',
                        style:
                            TextStyle(fontSize: 12, color: Color(0xffBEBEBE)),
                      ),
                    ],
                  ))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
