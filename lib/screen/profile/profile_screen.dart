// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:mobiletest/helper/constants.dart';
import 'package:mobiletest/widget/animated_toggle.dart';
import 'package:mobiletest/widget/custom_text.dart';
import 'package:mobiletest/widget/custom_text_field_form.dart';
import 'package:mobiletest/widget/profile_widget.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final GlobalKey<ScaffoldState> _endDrawerKey = GlobalKey();
  int _toggleValue = 0;

  List<String> list = [
    'All Product',
    'Layanan Kesehatan',
    'Alat Kesehatan',
    'Obat - obatan'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _endDrawerKey,
      endDrawerEnableOpenDragGesture: true,
      endDrawer: const Drawer(
        child: ProfileDrawerWidget(),
      ),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.menu,
                color: CustomColors.BASE_COLOR,
              ),
              onPressed: () {
                _endDrawerKey.currentState!.openEndDrawer();
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
        actions: [
          const Icon(
            Icons.shopping_cart,
            color: CustomColors.BASE_COLOR,
          ),
          const SizedBox(
            width: 20.0,
          ),
          Container(
              margin: const EdgeInsets.only(right: 16),
              child: const Icon(
                Icons.notifications,
                color: CustomColors.BASE_COLOR,
              )),

          // Icon(Icons.abc),
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            AnimatedToggle(
              values: const ['Profile Saya', 'Pengaturan'],
              onToggleCallback: (value) {
                setState(() {
                  _toggleValue = value;
                });
              },
              buttonColor: CustomColors.TOGGLE,
              backgroundColor: CustomColors.WHITE,
              textColor: CustomColors.BASE_COLOR,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              // height: 600,
              margin: const EdgeInsets.only(
                  top: 16, left: 30, right: 30, bottom: 16),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.1),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: const Offset(0, 3), // changes position of shadow
                    ),
                  ],
                  borderRadius: BorderRadius.circular(20),
                  color: CustomColors.WHITE),
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 156,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        image: const DecorationImage(
                            image: AssetImage('assets/images/pola.png'),
                            fit: BoxFit.cover),
                        color: CustomColors.BASE_COLOR2),
                    child: Stack(children: [
                      const Positioned(
                          top: 20,
                          left: 0,
                          child: SizedBox(
                              width: 300,
                              child: ListTile(
                                leading: CircleAvatar(
                                  backgroundImage: NetworkImage(
                                    "https://i.ibb.co/PGv8ZzG/me.jpg",
                                  ),
                                ),
                                title: Text("Angga Praja",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 17,
                                        color: CustomColors.WHITE)),
                                subtitle: Text(
                                  "Membership BBLK",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14,
                                      color: CustomColors.GREY),
                                ),
                              ))),
                      Positioned(
                        bottom: -5,
                        left: -11,
                        child: Container(
                          width: 300,
                          // height: 600,
                          margin: const EdgeInsets.only(
                              top: 16, left: 30, right: 30, bottom: 16),
                          child: const Text(
                            'Lengkapi profile anda untuk memaksimalkan penggunaan aplikasi',
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 14,
                                color: CustomColors.WHITE),
                          ),
                        ),
                      )
                    ]),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text('Pilih data yang ingin ditampilkan',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: CustomColors.BASE_COLOR)),
                        const SizedBox(
                          height: 20.0,
                        ),
                        ListTile(
                          leading: Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(60),
                                color: CustomColors.TOGGLE),
                            child: const Icon(
                              Icons.person_pin_rounded,
                              color: CustomColors.BASE_COLOR,
                            ),
                          ),
                          title: const Text("Data Diri",
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                              )),
                          subtitle: const Text("Data diri anda sesuai KTP",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 11.5,
                                  color: CustomColors.GREY)),
                          trailing: Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(60),
                                color: const Color(0xffEBEDF7)),
                            child: const Icon(
                              Icons.location_on,
                              color: Color(0xff6a6a6a),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                        CustomText(
                          text: 'Nama Depan',
                          color: CustomColors.BASE_COLOR,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                        const SizedBox(
                          height: 8.0,
                        ),
                        CustomTextFieldForm(
                          keyboardType: TextInputType.text,
                          // color: ,
                          isDense: false,
                          isAll: true,
                          hintText: 'Jhon',
                          // controller: _email,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Email tidak boleh kosong';
                            }
                            return '';
                          },
                          onSaved: (value) {
                            // _payload.fullName = value;
                          },
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                        CustomText(
                          text: 'Nama Belakang',
                          color: CustomColors.BASE_COLOR,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                        const SizedBox(
                          height: 8.0,
                        ),
                        CustomTextFieldForm(
                          keyboardType: TextInputType.text,
                          // color: ,
                          isDense: false,
                          isAll: true,
                          hintText: 'Doe',
                          // controller: _email,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Email tidak boleh kosong';
                            }
                            return '';
                          },
                          onSaved: (value) {
                            // _payload.fullName = value;
                          },
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                        CustomText(
                          text: 'Email',
                          color: CustomColors.BASE_COLOR,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                        const SizedBox(
                          height: 8.0,
                        ),
                        CustomTextFieldForm(
                          keyboardType: TextInputType.text,
                          // color: ,
                          isDense: false,
                          isAll: true,
                          hintText: 'Masukkan email anda',
                          // controller: _email,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Email tidak boleh kosong';
                            }
                            return '';
                          },
                          onSaved: (value) {
                            // _payload.fullName = value;
                          },
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                        CustomText(
                          text: 'No Telpon',
                          color: CustomColors.BASE_COLOR,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                        const SizedBox(
                          height: 8.0,
                        ),
                        CustomTextFieldForm(
                          keyboardType: TextInputType.text,
                          // color: ,
                          isDense: false,
                          isAll: true,
                          hintText: 'Masukkan No Telpon anda',
                          // controller: _email,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Email tidak boleh kosong';
                            }
                            return '';
                          },
                          onSaved: (value) {
                            // _payload.fullName = value;
                          },
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                        CustomText(
                          text: 'No. KTP',
                          color: CustomColors.BASE_COLOR,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                        const SizedBox(
                          height: 8.0,
                        ),
                        CustomTextFieldForm(
                          keyboardType: TextInputType.text,
                          // color: ,
                          isDense: false,
                          isAll: true,
                          hintText: 'Masukkan No KTP anda',
                          // controller: _email,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Email tidak boleh kosong';
                            }
                            return '';
                          },
                          onSaved: (value) {
                            // _payload.fullName = value;
                          },
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                        // const Text('data'),
                        const ListTile(
                          leading: Icon(Icons.info),
                          title: Text(
                              "Pastikan profile anda terisi dengan benar, data pribadi anda terjamin keamanannya",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 11,
                                  color: CustomColors.GREY)),
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                        Container(
                          // margin: const EdgeInsets.all(10),
                          padding: const EdgeInsets.only(right: 12),
                          width: double.infinity,
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: const Color(0xff002060)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              const Text('Simpan Profile',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600)),
                              SizedBox(
                                width: MediaQuery.of(context).size.width / 3.5,
                              ),
                              const Icon(Icons.save_outlined,
                                  color: Colors.white)
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 107,
              width: MediaQuery.of(context).size.width,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/pola.png'),
                      fit: BoxFit.cover),
                  color: CustomColors.BASE_COLOR2),
              child: Stack(children: [
                Positioned(
                    top: 20,
                    right: 90,
                    child: Image.asset(
                      'assets/images/ornamen.png',
                      width: 40,
                      height: 42,
                    )),
                const Positioned(
                    top: 40,
                    left: 30,
                    child: SizedBox(
                      width: 172,
                      child: Text('Ingin mendapat update dari kami ?',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: CustomColors.WHITE)),
                    )),
                Positioned(
                    top: 40,
                    right: -20,
                    child: SizedBox(
                        width: 172,
                        child: TextButton.icon(
                          onPressed: () {},
                          icon: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: const [
                              Text('Dapatkan',
                                  style: TextStyle(
                                      color: CustomColors.WHITE,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400)),
                              Text(' Aplikasi',
                                  style: TextStyle(
                                      color: CustomColors.WHITE,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400)),
                            ],
                          ),
                          label: const Icon(
                            Icons.arrow_forward,
                            size: 20,
                            color: CustomColors.WHITE,
                          ),
                        ))),
              ]),
            )
          ],
        ),
      ),
    );
  }
}
