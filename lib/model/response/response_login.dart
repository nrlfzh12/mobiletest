// To parse this JSON data, do
//
//     final responseLogin = responseLoginFromJson(jsonString);

import 'dart:convert';

class ResponseLogin {
  String token;

  ResponseLogin({
    required this.token,
  });

  ResponseLogin copyWith({
    String? token,
  }) =>
      ResponseLogin(
        token: token ?? this.token,
      );

  factory ResponseLogin.fromRawJson(String str) =>
      ResponseLogin.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ResponseLogin.fromJson(Map<String, dynamic> json) => ResponseLogin(
        token: json["token"],
      );

  Map<String, dynamic> toJson() => {
        "token": token,
      };
}
