// To parse this JSON data, do
//
//     final responseError = responseErrorFromJson(jsonString);

import 'dart:convert';

class ResponseError {
  String error;

  ResponseError({
    required this.error,
  });

  ResponseError copyWith({
    String? error,
  }) =>
      ResponseError(
        error: error ?? this.error,
      );

  factory ResponseError.fromRawJson(String str) =>
      ResponseError.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ResponseError.fromJson(Map<String, dynamic> json) => ResponseError(
        error: json["error"],
      );

  Map<String, dynamic> toJson() => {
        "error": error,
      };
}
