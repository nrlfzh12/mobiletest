// To parse this JSON data, do
//
//     final requestLogin = requestLoginFromJson(jsonString);

import 'dart:convert';

class RequestLogin {
  String email;
  String password;

  RequestLogin({
    required this.email,
    required this.password,
  });

  RequestLogin copyWith({
    String? email,
    String? password,
  }) =>
      RequestLogin(
        email: email ?? this.email,
        password: password ?? this.password,
      );

  factory RequestLogin.fromRawJson(String str) =>
      RequestLogin.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory RequestLogin.fromJson(Map<String, dynamic> json) => RequestLogin(
        email: json["email"],
        password: json["password"],
      );

  Map<String, dynamic> toJson() => {
        "email": email,
        "password": password,
      };
}
