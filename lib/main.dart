import 'package:flutter/material.dart';
import 'package:mobiletest/bloc/auth/auth_bloc.dart';
import 'package:mobiletest/bloc/obsecure/obsecure_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobiletest/helper/navigation.dart';
import 'package:mobiletest/helper/router.dart';
import 'package:mobiletest/screen/splash/splash_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => AuthBloc(),
        ),
        BlocProvider(
          create: (context) => ObsecureBloc(),
        ),
      ],
      child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            fontFamily: 'Gilroy',
            primarySwatch: Colors.blue,
          ),
          routes: myRoutes,
          navigatorKey: myNavigatorKey,
          home: const SplashScreen()),
    );
  }
}
