import 'dart:convert';
import 'dart:developer';
import 'package:dio/dio.dart';
import 'package:mobiletest/util/preference_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NetworkHelper {
  static const String divider = "\n------------------------------------";
  static final Dio _dio = Dio();
  static const TOKEN = "Token";
  static const _connectTimeoutDefault = Duration(seconds: 60);
  static const _receiveTimeoutDefault = Duration(seconds: 60);
  static const _sendTimeoutDefault = Duration(seconds: 60);

  String _formDataToJson(FormData formData) {
    final fields = formData.fields;
    final files = formData.files;
    final map = <String, String>{};

    for (MapEntry<String, String> field in fields) {
      map[field.key] = field.value;
    }

    for (MapEntry<String, MultipartFile> file in files) {
      map[file.key] = file.value.filename!;
    }

    return json.encode(map);
  }

  Future<Response> get(String endpoint,
      {bool isUseToken = true,
      Map<String, dynamic>? params,
      Function(int, int)? progress,
      Options? options}) async {
    PreferencesHelper preferencesHelper =
        PreferencesHelper(sharedPreferences: SharedPreferences.getInstance());

    _dio.options.headers['Authorization'] =
        'Bearer ${await preferencesHelper.getToken}';
    return await _dio.get(endpoint,
        queryParameters: params, onReceiveProgress: progress, options: options);
  }

  Future<Response> post(String endpoint,
      {bool isUseToken = true,
      Map<String, dynamic>? params,
      Function(int, int)? receiveProgress,
      Function(int, int)? progress,
      Options? options,
      dynamic data}) async {
    PreferencesHelper preferencesHelper =
        PreferencesHelper(sharedPreferences: SharedPreferences.getInstance());

    String token = await preferencesHelper.getToken;
    log("token $token");
    _dio.options.headers['Authorization'] = 'Bearer $token';
    return await _dio.post(endpoint,
        queryParameters: params,
        onReceiveProgress: receiveProgress,
        options: options,
        onSendProgress: progress,
        data: data);
  }

  Future<Response> postFormData(String endpoint,
      {bool isUseToken = true,
      Map<String, dynamic>? params,
      Function(int, int)? receiveProgress,
      Function(int, int)? progress,
      Options? options,
      dynamic data}) async {
    PreferencesHelper preferencesHelper =
        PreferencesHelper(sharedPreferences: SharedPreferences.getInstance());

    String token = await preferencesHelper.getToken;
    log("token $token");
    _dio.options.headers['Authorization'] = 'Bearer $token';
    _dio.options.headers['Content-Type'] = 'multipart/form-data';
    return await _dio.post(endpoint,
        queryParameters: params,
        onReceiveProgress: receiveProgress,
        options: options,
        onSendProgress: progress,
        data: data);
  }

  Future<Response> put(String endpoint,
      {bool isUseToken = true,
      Map<String, dynamic>? params,
      Function(int, int)? receiveProgress,
      Function(int, int)? progress,
      Options? options,
      dynamic data}) async {
    PreferencesHelper preferencesHelper =
        PreferencesHelper(sharedPreferences: SharedPreferences.getInstance());

    _dio.options.headers['Authorization'] =
        'Bearer ${await preferencesHelper.getToken}';
    return await _dio.put(endpoint,
        queryParameters: params,
        onReceiveProgress: receiveProgress,
        onSendProgress: progress,
        options: options,
        data: data);
  }

  Future<Response> delete(String endpoint,
      {bool isUseToken = true,
      Map<String, dynamic>? params,
      Options? options,
      dynamic data}) async {
    PreferencesHelper preferencesHelper =
        PreferencesHelper(sharedPreferences: SharedPreferences.getInstance());

    _dio.options.headers['Authorization'] =
        'Bearer ${await preferencesHelper.getToken}';
    return await _dio.delete(endpoint,
        queryParameters: params, options: options, data: data);
  }

  Future<Response> download(String endpoint,
      {bool isUseToken = true,
      savePath,
      dynamic data,
      Options? options,
      Function(int, int)? progress,
      Map<String, dynamic>? params}) async {
    PreferencesHelper preferencesHelper =
        PreferencesHelper(sharedPreferences: SharedPreferences.getInstance());

    _dio.options.headers['Authorization'] =
        'Bearer ${await preferencesHelper.getToken}';
    return await _dio.download(endpoint, savePath,
        data: data,
        options: options,
        onReceiveProgress: progress,
        queryParameters: params);
  }

  // static Future<Dio> client(
  //     {isUseToken = true,
  //     Duration? connectTimeout,
  //     Duration? receiveTimeout,
  //     Duration? sendTimeout}) async {
  //   final client = Dio();

  //   String token;
  //   if (isUseToken) {
  //     token = await LocalHelper.getSession(TOKEN, defaultValue: null);
  //   } else {
  //     token = '';
  //   }
  //   client.interceptors.add(interceptor(token,
  //       connectTimeout: connectTimeout!,
  //       receiveTimeout: receiveTimeout!,
  //       sendTimeout: sendTimeout!));
  //   return client;
  // }

//   @deprecated
//   static Dio clientCrud() {
//     final client = Dio();
//     client.interceptors.add(crudInterceptor());
//     return client;
//   }

//   @deprecated
//   static Dio clientUpload() {
//     final client = Dio();
//     client.interceptors.add(uploadInterceptor());
//     return client;
//   }

//   static InterceptorsWrapper interceptor(String token,
//       {Duration connectTimeout = _connectTimeoutDefault,
//       Duration receiveTimeout = _receiveTimeoutDefault,
//       Duration sendTimeout = _sendTimeoutDefault}) {
//     return InterceptorsWrapper(onRequest:
//         (RequestOptions options, RequestInterceptorHandler interceptorHandler) {
//       options.connectTimeout =
//           (connectTimeout != null) ? connectTimeout.inMilliseconds : 60000;
//       options.receiveTimeout =
//           (connectTimeout != null) ? connectTimeout.inMilliseconds : 60000;
//       options.sendTimeout =
//           (connectTimeout != null) ? connectTimeout.inMilliseconds : 60000;
//       options.headers["Authorization"] = token;
//       options.headers["token"] = token;
//       final data = (options.data is FormData)
//           ? _formDataToJson(options.data as FormData)
//           : json.encode(options.data);

//       var logRequest =
//           "$divider\nRequest (${options.method}) : ${options.path} \n[Headers] : ${json.encode(options.headers)} \n[Params] : ${json.encode(options.queryParameters)} \n[Body] : $data \n$divider";
//       print(logRequest);

//       return options;
//     }, onResponse: (Response response,
//         ResponseInterceptorHandler responseInterceptorHandler) {
//       var logResponse =
//           "$divider\nResponse (${response.statusCode}) : ${json.encode(response.data)}\n$divider";
//       print(logResponse);

//       return response;
//     }, onError:
//         (DioError error, ErrorInterceptorHandler errorInterceptorHandler) {
//       var logError = "$divider\nError : \nMessage : ${error.message}";

//       print(logError);

//       if (error.response != null) {
//         var errorResponse =
//             "Request : ${error.response.toString()}Headers : ${error.response!.headers.toString()}Params: ${error.response.toString()}Data : ${json.encode(error.response.data)}";
//         print(errorResponse);
//       }

//       print("\n$divider");
//       return error;
//     });
//   }

//   @deprecated
//   static InterceptorsWrapper crudInterceptor() {
//     return InterceptorsWrapper(onRequest: (RequestOptions options,
//         RequestInterceptorHandler requestInterceptorHandler) {
//       final token = LocalHelper.getSession(TOKEN, defaultValue: null);

//       options.connectTimeout = 60000; //60s
//       options.receiveTimeout = 60000;
//       options.sendTimeout = 60000;
//       if (token != null) options.headers["Authorization"] = token;
//       if (token != null) options.headers["token"] = token;
//       final data = (options.data is FormData)
//           ? _formDataToJson(options.data as FormData)
//           : json.encode(options.data);

//       var logRequest =
//           "$divider\nRequest (${options.method}) : ${options.path} \n[Headers] : ${json.encode(options.headers)} \n[Params] : ${json.encode(options.queryParameters)} \n[Body] : $data \n$divider";
//       print(logRequest);

//       return options;
//     }, onResponse: (Response response,
//         ResponseInterceptorHandler responseInterceptorHandler) {
//       var logResponse =
//           "$divider\nResponse (${response.statusCode}) : ${json.encode(response.data)}\n$divider";
//       print(logResponse);

//       return response;
//     }, onError: (DioError error, ErrorInterceptorHandler) {
//       var logError = "$divider\nError : \nMessage : ${error.message}";

//       print(logError);

//       if (error.response != null) {
//         var errorResponse =
//             "Request : ${error.response.request.toString()}Headers : ${error.response.headers.toString()}Params: ${error.response.toString()}Data : ${json.encode(error.response.data)}";
//         print(errorResponse);
//       }

//       print("\n$divider");
//       return error;
//     });
//   }

//   @deprecated
//   static InterceptorsWrapper uploadInterceptor() {
//     return InterceptorsWrapper(onRequest: (RequestOptions options,
//         RequestInterceptorHandler requestInterceptorHandler) {
//       final token = LocalHelper.getSession(TOKEN, defaultValue: null);

//       options.connectTimeout = 120000; //2m
//       options.receiveTimeout = 120000;
//       options.sendTimeout = 120000;
//       if (token != null) options.headers["Authorization"] = token;
//       if (token != null) options.headers["token"] = token;
//       final data = (options.data is FormData)
//           ? _formDataToJson(options.data as FormData)
//           : options.data.toString();

//       var logRequest =
//           "Request (${options.method}) : ${options.path} \n[Headers] : ${json.encode(options.headers)} \n[Params] : ${json.encode(options.queryParameters)} \n[Body] : $data \n";
//       print(logRequest);

//       return options;
//     }, onResponse: (Response response, ResponseInterceptorHandler) {
//       var logResponse =
//           "Response (${response.statusCode}) : ${json.encode(response.data)}";
//       print(logResponse);

//       return response;
//     }, onError: (DioError error) {
//       var logError = "Error : \n"
//           "Message : ${error.message}";

//       print(logError);

//       if (error.response != null) {
//         var errorResponse =
//             "Response : ${error.response.request.toString()}Headers : ${json.encode(error.response.headers.toString())}Data : ${json.encode(error.response.data)}";
//         print(errorResponse);
//       }
//       return error;
//     });
//   }
// }

// extension NetworkError on DioError {
//   Map<String, dynamic> toJson() {
//     if (CommonUtil.isJsonValid(response.data)) {
//       return response.data;
//     } else {
//       if (response.data is Map) {
//         return json.decode(json.encode(response.data));
//       } else {
//         if (CommonUtil.isJsonValid(response.data)) {
//           return json.decode(response.data);
//         } else {
//           final contentJson = <String, dynamic>{};
//           contentJson["code"] = response.statusCode;
//           contentJson["data"] = null;
//           contentJson["message"] = response.statusMessage;
//           contentJson["error"] = message;

//           return json.decode(json.encode(contentJson));
//         }
//       }
//     }
//   }
// }
}
